﻿using Microsoft.AspNetCore.Mvc;
using Keepnote.Repository;
using System.Collections.Generic;
using Keepnote.Models;
using Keepnote_Step1_boilerplate.Models;

namespace Keepnote.Controllers
{
    public class NoteController : Controller
    {
        /*
	     From the problem statement, we can understand that the application
	     requires us to implement the following functionalities.
	     1. display the list of existing notes from the collection. Each note 
	        should contain Note Id, title, content, status and created date.
	     2. Add a new note which should contain the title, content and status.
	     3. Delete an existing note.
         4. Update an existing Note.
	    */
        private readonly NoteRepository noteRepository;
        //Inject the noteRepository instance through constructor injection.
        public NoteController()
        {
            this.noteRepository = new NoteRepository();
        }
        /*
      * Define a handler method to read the existing notes from the database and add
      * it to the ModelMap which is an implementation of Map, used when building
      * model data for use with views. it should map to the default URL i.e. "/index"
      */
        public IActionResult Index()
        {
            List<NoteModel> noteModels = new List<NoteModel>();
            List<Note> notes = noteRepository.GetAllNotes();
            foreach (Note note in notes)
            {
                noteModels.Add(new NoteModel() { NoteId = note.NoteId, NoteContent = note.NoteContent, NoteName = note.NoteTitle, NoteStatus = note.NoteStatus });
            }
            return View(noteModels);
        }

        /*
         * Define a handler method which will read the NoteTitle, NoteContent,
         * NoteStatus from request parameters and save the note in note table in
         * database. Please note that the CreatedAt should always be auto populated with
         * system time and should not be accepted from the user. Also, after saving the
         * note, it should show the same along with existing messages. Hence, reading
         * note has to be done here again and the retrieved notes object should be sent
         * back to the view. This handler method should map to the URL "/create".
         */
        public IActionResult Create()
        {
            return View();
        }
        [HttpPost]
        public IActionResult Create(NoteModel noteModel)
        {
            if (ModelState.IsValid)
            {
                Note note = new Note()
                {
                    NoteId = noteModel.NoteId,
                    NoteContent = noteModel.NoteContent,
                    NoteStatus = noteModel.NoteStatus,
                    NoteTitle = noteModel.NoteName,
                    CreatedAt = noteModel.CreatedDate
                };
                this.noteRepository.AddNote(note);
                return RedirectToAction("Index");
            }
            else
            {
                return View();
            }
        }


        /*
         * Define a handler method which will read the NoteId from request parameters
         * and remove an existing note by calling the deleteNote() method of the
         * NoteRepository class.".
         */
        public IActionResult Delete(int id)
        {
            noteRepository.DeletNote(id);
            return RedirectToAction("Index");
        }
        /*
         * Define a handler method which will update the existing note.
         */
        public IActionResult Edit(int id)
        {
            Note note = noteRepository.GetNoteById(id);
            NoteModel noteModel = new NoteModel()
            {
                NoteContent = note.NoteContent,
                NoteId = note.NoteId,
                NoteName = note.NoteTitle,
                NoteStatus = note.NoteStatus,
            };
            return View(noteModel);
        }
        [HttpPost]
        public IActionResult Edit(NoteModel noteModel)
        {
            if (ModelState.IsValid)
            {
                Note note = new Note()
                {
                    NoteId = noteModel.NoteId,
                    NoteContent = noteModel.NoteContent,
                    NoteStatus = noteModel.NoteStatus,
                    NoteTitle = noteModel.NoteName,
                    CreatedAt = noteModel.CreatedDate
                };
                this.noteRepository.UpdateNote(note);
                return RedirectToAction("Index");
            }
            else
            {
                return View();
            }
        }
    }
}