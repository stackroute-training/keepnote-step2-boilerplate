﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
namespace Keepnote_Step1_boilerplate.Models
{
    public class NoteModel
    {
        public int NoteId { get; set; }
        public string NoteName { get; set; }    
        public string NoteContent { get; set; }
        public string NoteStatus { get; set; }
        public DateTime CreatedDate { get { return DateTime.Now; } }

    }
}
