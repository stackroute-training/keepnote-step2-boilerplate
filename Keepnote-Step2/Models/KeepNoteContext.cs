﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Linq; 
namespace Keepnote.Models
{
    public class KeepNoteContext: DbContext
    {
       /*
        This class should be used as DbContext to speak to database and should make the use of Code first approach.
        It should autogenerate the database based upon the model class in your application
         */
       public DbSet<Note> Notes { get; set;}

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseSqlServer("Server=localhost;Database=KeepNotes;Trusted_Connection=True;");
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Note>(entity =>
            {
                entity.ToTable("Note");
                entity.HasKey(s => s.NoteId);
                entity.Property(s => s.NoteId).HasColumnName("id");
                entity.Property(s => s.NoteTitle).HasColumnName("title").IsRequired().HasMaxLength(20);
                entity.Property(s => s.NoteContent).HasColumnName("content").HasMaxLength(200).IsRequired();
                entity.Property(s => s.NoteStatus).HasColumnName("status").IsRequired().HasMaxLength(20);
                entity.Property<DateTime>("CreatedAt");
            });
        }
    }
}
