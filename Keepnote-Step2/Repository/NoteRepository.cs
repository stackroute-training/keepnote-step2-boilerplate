﻿using System.Collections.Generic;
using System.Linq;
using Keepnote.Models;
using Microsoft.EntityFrameworkCore;

namespace Keepnote.Repository
{
    public class NoteRepository : INoteRepository
    {

        // Save the note in the database(note) table.
        
        private readonly KeepNoteContext context;

        public NoteRepository()
        {
            
            this.context = new KeepNoteContext();
        }
        
        public void AddNote(Note note)
        {
            context.Notes.Add(note);
            context.SaveChanges();
        }
        //Remove the note from the database(note) table.
        public void DeletNote(int noteId)
        {
            Note note = this.context.Notes.Find(noteId);
            this.context.Notes.Remove(note);
            context.SaveChanges();
        }
        
        //can be used as helper method for controller
        public bool Exists(int noteId)
        {
            Note note = this.context.Notes.Find(noteId);
            if(note != null)
            {
                return true;
            }
            return false;
        }

       /* retrieve all existing notes sorted by created Date in descending
        order(showing latest note first)*/
        public List<Note> GetAllNotes()
        {
            return this.context.Notes.ToList();
        }

        //retrieve specific note from the database(note) table
        public Note GetNoteById(int noteId)
        {
            return this.context.Notes.Find(noteId);
        }
        //Update existing note
        public void UpdateNote(Note note)
        {
            this.context.Notes.Update(note);
            context.SaveChanges();
        }
    }
}
