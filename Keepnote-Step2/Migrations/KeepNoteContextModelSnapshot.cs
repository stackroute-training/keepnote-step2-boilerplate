﻿// <auto-generated />
using System;
using Keepnote.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Storage.ValueConversion;

namespace Keepnote_Step1_boilerplate.Migrations
{
    [DbContext(typeof(KeepNoteContext))]
    partial class KeepNoteContextModelSnapshot : ModelSnapshot
    {
        protected override void BuildModel(ModelBuilder modelBuilder)
        {
#pragma warning disable 612, 618
            modelBuilder
                .HasAnnotation("ProductVersion", "2.2.6-servicing-10079")
                .HasAnnotation("Relational:MaxIdentifierLength", 128)
                .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

            modelBuilder.Entity("Keepnote.Models.Note", b =>
                {
                    b.Property<int>("NoteId")
                        .ValueGeneratedOnAdd()
                        .HasColumnName("id")
                        .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

                    b.Property<DateTime>("CreatedAt");

                    b.Property<string>("NoteContent")
                        .IsRequired()
                        .HasColumnName("content")
                        .HasMaxLength(200);

                    b.Property<string>("NoteStatus")
                        .IsRequired()
                        .HasColumnName("status")
                        .HasMaxLength(20);

                    b.Property<string>("NoteTitle")
                        .IsRequired()
                        .HasColumnName("title")
                        .HasMaxLength(20);

                    b.HasKey("NoteId");

                    b.ToTable("Note");
                });
#pragma warning restore 612, 618
        }
    }
}
